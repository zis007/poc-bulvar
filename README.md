# POC Bulvar

We will need to adjust more color variables which can be done on original implementation. In this repo I just updated the black, dark, white and light colors.

The main idea here is, we have modified the bulma from bulvar using sass variable that will match our (default)dark theme.

Then I used the css variables that I get from bulvar and updated with colors for the light theme.

This way we can use the already defined classes and just toggle the root level class `light-theme`.

![dark-light-mode](/uploads/bca27bfce6b96bd27165ce59c9daa4a3/dark-light-mode.gif)



## Run the project

- Go to the repo
- run `yarn` if for the first time
- run `yarn serve`

