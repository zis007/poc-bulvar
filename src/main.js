import Vue from "vue";
import App from "./App.vue";

require("@/assets/main.scss");
require("@/assets/light.scss");

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");

const htmlTag = document.querySelector("html");
const buttonLight = document.getElementById("lightMode");

buttonLight.addEventListener("click", () => {
  htmlTag.classList.toggle("light-theme");
});
